# LATEX for scientific documents



## Description

This is a support for the course "_LaTeX for scientific documents (PhD thesis, articles, slides)_" ([PACT 50](https://secodoc.emse.fr/formations/pact/4033/)) proposed in the framework of the doctoral program of the [Ecole Doctorale 488 SIS](https://edsis.universite-lyon.fr/).

## General-purpose ressources

- [ ] **Donald Knuth**'s lectures: https://www.youtube.com/playlist?list=PL94E35692EB9D36F3
- [ ] [Awesome LaTeX](https://github.com/egeerardyn/awesome-LaTeX#blogs): A curation including
    * **Learning ressources**: tutorials, blogs, social media, etc.
    * Assistant **tools for writing** in LaTeX: distributions, engines, math, tables, graphics, bibliography, etc.
- [ ] [Mathcha](https://www.mathcha.io/): An online mathematics editor allowing users to write **mathematics**, import/export LaTeX for math mode, draw **graphs/diagrams**, and export to SVG or **Tikz format**
- [ ] [texample.net](https://texample.net): A gallery of PGF and TikZ examples

## Specific-purpose ressources

- [ ] **Thesis** manuscript template: https://www.overleaf.com/read/sdjvhstzfrry
- [ ] Beamer **presentation** template: 
    * https://www.overleaf.com/3814232225wfzymvphxxqj
    * https://gitlab.emse.fr/theophile.gousselot/emse_beamer_template
- [ ] **Alternative text** and accesibility test: https://www.overleaf.com/read/kvhzkqxtzzkq
